const express = require('express')            // javascript router
const jwt = require('jsonwebtoken')           // ???
const bcrypt = require('bcrypt')              // generate and read hashed passwords
const bodyParser = require('body-parser')     // body content parser, json, html, etc.
const fs = require('fs')                      // access to the local file system
const cookieParser = require('cookie-parser') // access the user browser's cookies

const PORT = 3000
const SECRET_KEY = 'my_secret_key'
const SALT_ROUNDS = 10
const app = express()

// in this example, the database is a mediocry javascript object
const usersDB = [
    {
        id: 0x1,
        username: 'user1_normal',
        password: '$2b$10$OOXaLhrQwgjHazeFsIaRoebrxme1YC77Mhr2PRQ5KoEywvShSWjUm', // (password1)
        role: 'user'
    },
    {
        id: 0x2,
        username: 'user2_admin',
        password: '$2b$10$ZbyZ113i2vI08srR0XeL4edZ5HeaK.f5F/h9wcK5wVhP18WJRgXxO', // (password2)
        role: 'admin'
    }
]

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(bodyParser.json())

// login endpoint
app.post('/login', (req, res) => {
    const { username, password } = req.body
    const user = usersDB.find(u => u.username === username)

    if (!user || !bcrypt.compareSync(password,user.password))
        return res.status(401).json({ message: 'Invalid username or password' })

    const token = jwt.sign({ id: user.id, username: user.username,
        role: user.role }, SECRET_KEY, { expiresIn: '1m' })

    res.cookie('authorization', token, { httpOnly: true })
    res.redirect('/')
})

// protected endpoint
app.get('/protected', (req, res) => {
    const token = req.cookies['authorization']

    jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) return res.redirect('/login-form')

        res.json({ message: 'Protected resource of the system. Congratulations!',
            decoded })
    })
})

// login-form page
app.get('/login-form', (_req, res) => {
    fs.readFile('login-form.html', 'utf-8', (err, html) => {
        if (err)
            res.status(500).send('Internal Server Error')
        else
            res.send(html)
    })
})

// main route that redirects to the others
app.get('/', (req, res) => {
    const token = req.cookies['authorization']

    if (!token)
        return res.redirect('/login-form')

    res.redirect('/protected')
})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})
